#include "RandomForest.h"
#include <random>
#include <stdexcept>
#include <algorithm>
#include "../../utils/sampling.h"
#include "../../utils/maps.h"

//TODO: remove commented lines after testing
//TODO: Possibles optimisations:
//  - if number_to_add is small, draw points indices at random and then get points (unordered_set could help): complexity O(klog(n)) or even O(k) (unordered_set)
void RandomForest::complete_tree(Tree& tree_to_complete, std::map<Point*, int>& map_of_tree, size_t number_to_add)
{
    std::random_device device;
    std::default_random_engine gen(device());
    size_t point_index = 0;
    std::map<Point*, int> points_to_add;
    for(auto points_it = this->points.begin();
            points_it != this->points.end() && number_to_add;
            points_it++, point_index++)
    {
        // TODO: Check if Poisson better
        std::binomial_distribution<int> distrib(number_to_add,//-sum,
                                                (float)1/(this->points.size() - point_index));
        int point_occurences = distrib(gen);
        if(point_occurences)
        {
            points_to_add.insert(points_to_add.end(), std::pair<Point*, int>(*points_it, point_occurences));
            number_to_add -= point_occurences;
            // TODO: Replace this with batch addition of points
            for(int i = 0; i < point_occurences; i++)
                tree_to_complete.add_point(new Point(**points_it));
        }
    }
    merge_sum_inplace(map_of_tree, points_to_add);
}

RandomForest::RandomForest(std::multiset<Point*> list_of_points, unsigned int nb_trees, unsigned int samples_size,
                     unsigned int max_height, float epsilon, unsigned int min_split_points, float min_split_gini,
                     float epsilon_transmission, std::vector<FeatureType> features_types, size_t feat_samp_size):
    trees(),
    points_of_trees(nb_trees, std::map<Point*, int>()),
    points(list_of_points.begin(), list_of_points.end()),
    max_height(max_height),
    samples_size(samples_size),
    epsilon(epsilon),
    min_split_points(min_split_points),
    min_split_gini(min_split_gini),
    epsilon_transmission(epsilon_transmission),
    feat_samp_size(feat_samp_size),
    features_types(features_types)
{
    this->trees.reserve(nb_trees);
    std::vector<Point*> temporary_ordered_storage(this->points.begin(), this->points.end());

    std::random_device device;
    std::default_random_engine gen(device());
    // Number of points already drawn
    // TODO: Adapt with number_to_add
    std::vector<int> sum(nb_trees, 0);
    std::vector<std::multiset<Point*>> trees_points(nb_trees, std::multiset<Point*>());
    size_t point_index = 0;
    for(auto points_it = this->points.begin();
            points_it != this->points.end();
            points_it++, point_index++)
    {
        auto sums_it = sum.begin();
        auto multisets_it = trees_points.begin();
        for(auto trees_it = this->points_of_trees.begin();
                trees_it != this->points_of_trees.end();
                trees_it++, sums_it++, multisets_it++)
        {
            std::binomial_distribution<int> distrib(samples_size-*sums_it,
                                                    (float)1/(this->points.size() - point_index));
            int point_occurences = distrib(gen);
            if(point_occurences)
            {
                (*trees_it)[*points_it] = point_occurences;
                *sums_it += point_occurences;
                for(int i = 0; i < point_occurences; i++)
                    multisets_it->insert(new Point(**points_it));
            }
        }
    }
    for(auto trees_it = trees_points.begin(); trees_it != trees_points.end(); trees_it++)
    {
        this->trees.push_back(Tree(*trees_it, features_types.size(), max_height,
                        epsilon, min_split_points, min_split_gini,
                        epsilon_transmission, features_types, feat_samp_size)); 
    }
}

RandomForest::RandomForest(const RandomForest& source):
   trees(source.trees.begin(), source.trees.end()),
   points_of_trees(source.trees.size(), std::map<Point*, int>()),
   points(),
   max_height(source.max_height),
   samples_size(source.samples_size),
   epsilon(source.epsilon),
   min_split_points(source.min_split_points),
   min_split_gini(source.min_split_gini),
   epsilon_transmission(source.epsilon_transmission),
   feat_samp_size(source.feat_samp_size),
   features_types(features_types)
{
    std::map<Point*, Point*> old_to_new;
    for(auto it = source.points.begin(); it != source.points.end(); it++)
    {
        Point* new_point = new Point(**it);
        old_to_new[*it] = new_point;
        this->points.insert(new_point);
    }
    auto this_treepoints_it = this->points_of_trees.begin();
    for(auto it = source.points_of_trees.begin();
            it != source.points_of_trees.end();
            it++, this_treepoints_it++)
    {
        for(auto point_it = it->begin(); point_it != it->end(); point_it++)
            (*this_treepoints_it)[old_to_new[point_it->first]] = point_it->second;
    }
}

RandomForest::RandomForest(const RandomForest& source, const float& epsilon, const float& epsilon_transmission):
   trees(),
   points_of_trees(source.trees.size(), std::map<Point*, int>()),
   points(),
   max_height(source.max_height),
   samples_size(source.samples_size),
   epsilon(epsilon),
   min_split_points(source.min_split_points),
   min_split_gini(source.min_split_gini),
   epsilon_transmission(epsilon_transmission),
   feat_samp_size(source.feat_samp_size),
   features_types(features_types)
{
    this->trees.reserve(source.trees.size());

    // Copy points
    std::map<Point*, Point*> old_to_new;
    for(auto it = source.points.begin(); it != source.points.end(); it++)
    {
        Point* new_point = new Point(**it);
        old_to_new[*it] = new_point;
        this->points.insert(new_point);
    }

    // Remap points_of_trees
    // "it" and "this treepoints_it" move together, the first on the source points_of_trees
    // and the second on the new object points_of_trees
    auto this_treepoints_it = this->points_of_trees.begin();
    for(auto it = source.points_of_trees.begin();
            it != source.points_of_trees.end();
            it++, this_treepoints_it++)
    {
        for(auto point_it = it->begin(); point_it != it->end(); point_it++)
            (*this_treepoints_it)[old_to_new[point_it->first]] = point_it->second;
    }

    // Re-create the trees with the modified epsilon value
    for(auto tree_it = source.trees.begin(); tree_it != source.trees.end(); tree_it++)
        this->trees.push_back(Tree(*tree_it, epsilon, epsilon_transmission));
}

RandomForest::~RandomForest()
{
	for(auto it = this->points.begin(); it != this->points.end(); it++)
		delete *it;
}

RandomForest& RandomForest::operator=(const RandomForest& source)
{
    this->trees = std::vector<Tree>(source.trees.begin(), source.trees.end());
    this->points_of_trees = std::vector<std::map<Point*, int>>(source.trees.size(), std::map<Point*, int>());
    this->max_height = source.max_height;
    this->epsilon = source.epsilon;
    this->min_split_points = source.min_split_points;
    this->min_split_gini = source.min_split_gini;
    this->epsilon_transmission = source.epsilon_transmission;
    this->feat_samp_size = source.feat_samp_size;
    std::map<Point*, Point*> old_to_new;
    for(auto it = source.points.begin(); it != source.points.end(); it++)
    {
        Point* new_point = new Point(**it);
        old_to_new[*it] = new_point;
        this->points.insert(new_point);
    }
    auto this_treepoints_it = this->points_of_trees.begin();
    for(auto it = source.points_of_trees.begin();
            it != source.points_of_trees.end();
            it++, this_treepoints_it++)
    {
        for(auto point_it = it->begin(); point_it != it->end(); point_it++)
            (*this_treepoints_it)[old_to_new[point_it->first]] = point_it->second;
    }
    return *this;
}

void RandomForest::add_point(const float* features, bool value)
{
   this->add_point(new Point(this->features_types.size(), features, value));
}

void RandomForest::add_point(Point* to_add)
{
    this->points.insert(to_add);

    std::random_device device;
    std::default_random_engine gen(device());
    std::binomial_distribution<int> distrib_new(this->samples_size,
                                            (float)1/(this->points.size()));

    // The two iterators work together:
    // tree_it iterates over the trees,
    // and point_of_trees over the points to occurences maps for each tree
    auto tree_it = this->trees.begin();
    for(auto points_map_it = this->points_of_trees.begin();
            points_map_it != this->points_of_trees.end();
            points_map_it++, tree_it++)
    {
        int new_points_nb = distrib_new(gen);
        for(int i = 0; i<new_points_nb; i++)
            tree_it->add_point(*to_add);
        std::vector<int> points_to_remove = sample_without_replacement(0, samples_size-1, new_points_nb);
        auto del_id_it = points_to_remove.begin();
        // Number of points already studied (deleted or not)
        //  = index of the first of this point
        int already_seen = 0;
        // Vector of iterators to the points to erase from the dict (card down to 0)
        std::vector<std::map<Point*, int>::iterator> keys_to_erase;
        for(auto map_it = points_map_it->begin(); del_id_it != points_to_remove.end(); map_it++)
        {
            // TODO: It should be possible to remove that check when all will be correcly debugged
            if(map_it == points_map_it->end())
                throw std::runtime_error("Error: Unexpected try to delete more points than existing. The real size of the sample is likely less than the expected one");
            int remove_nb = 0;
            while(del_id_it != points_to_remove.end() && map_it->second + already_seen > *del_id_it)
            {
                remove_nb++; del_id_it++;
                tree_it->delete_point(*(map_it->first));
            }
            already_seen += map_it->second;
            map_it->second -= remove_nb;
            if(not map_it->second)
                keys_to_erase.push_back(map_it);
        }
        for(auto keys_it = keys_to_erase.begin(); keys_it != keys_to_erase.end(); keys_it++)
            points_map_it->erase(*keys_it);
        (*points_map_it)[to_add] = new_points_nb;
        for(int i = 0; i<new_points_nb; i++)
            tree_it->add_point(*to_add);
    }
}

void RandomForest::add_point(const Point& to_add)
{
    this->add_point(new Point(to_add));
}

void RandomForest::delete_point(Point& to_delete)
{
    // Pointer/iterator to the position of the point to delete in the points set
    auto it_to_delete = this->points.find(&to_delete);
    if(it_to_delete == this->points.end())
        throw std::runtime_error("Error : Point does not exists");
    std::vector<int> nb_per_tree;

    auto trees_it = this->trees.begin();
    for(auto map_it = this->points_of_trees.begin();
            map_it != this->points_of_trees.end();
            map_it++, trees_it++)
    {
        auto pair_to_del = map_it->find(*it_to_delete);
        if(pair_to_del != map_it->end())
        {
            for(int i = 0; i < pair_to_del->second; i++)
                trees_it->delete_point(**it_to_delete);
            nb_per_tree.push_back(pair_to_del->second);
            map_it->erase(pair_to_del);
        }
        else
            nb_per_tree.push_back(0);
    }
    delete *it_to_delete;
    this->points.erase(it_to_delete);
    trees_it = this->trees.begin();
    auto nb_it = nb_per_tree.begin();
    for(auto map_it = this->points_of_trees.begin();
            map_it != this->points_of_trees.end();
            map_it++, trees_it++, nb_it++)
        this->complete_tree(*trees_it, *map_it, *nb_it);
}

void RandomForest::delete_point(const float* features, bool value)
{
	Point pattern_to_delete(this->features_types.size(), features, value);
	this->delete_point(pattern_to_delete);
}

std::string RandomForest::to_string()
{
	std::vector<std::string> vec_of_res;
        for(auto trees_it = this->trees.begin(); trees_it != this->trees.end(); trees_it++)
            vec_of_res.push_back(trees_it->to_string());
	return std::accumulate(vec_of_res.begin(), vec_of_res.end(), std::string(""));
}

float RandomForest::get_positive_proportion(const float* features)
{
    unsigned int number_positive = 0;
    for(auto tree_it = this->trees.begin(); tree_it != this->trees.end(); tree_it++)
        number_positive += tree_it->decision(features);
    return (float)number_positive/this->trees.size();
}

bool RandomForest::decision(const float* features)
{
    return this->get_positive_proportion(features) > .5;
}

std::vector<unsigned int> RandomForest::get_training_errors()
{
    std::vector<unsigned int> result_vector;
    for(auto tree_it = this->trees.begin(); tree_it != this->trees.end(); tree_it++)
        result_vector.push_back(tree_it->get_training_error());
    return result_vector;
}

