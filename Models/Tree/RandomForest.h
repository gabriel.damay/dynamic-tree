/**
 * @file RandomForest.h
 * Definition of class RandomForest
 */
#ifndef RANDOMFOREST_H_INLCUDED
#define RANDOMFOREST_H_INCLUDED

#include <vector>
#include <set>
#include <map>
#include "Tree.h"
#include "../PointSet/Point.h"

/**
 * The Random Forest
 *
 * This class create and manage Trees, and deals with the
 * points sampling for each Tree.
 * @todo Make the Tree points belong to RandomForest
 */
class RandomForest {
    private:
        /**
         * Defines the relation between pointers to use multiset
         *
         * A pointer is leq to another if the underlying point is
         */
        struct point_ptr_compare
        {
            /**
             * Comparision operator for Point pointers
             *
             * A pointer is leq to another if the underlying point is.
             *
             * This is usefull for using multisets of pointers
             */
            bool operator()(const Point* left, const Point* right) const {return (*left < *right);}
        };

        /// The trees of the forest
        /// @todo Transform into pointers
        std::vector<Tree> trees;

        /**
         * Points of each Tree
         *
         * Each item correspond to a given tree, and the map maps each of its distinct points
         * to the number of occurences of this point in the tree
         * @todo move this mapping into the Tree object
         */
        std::vector<std::map<Point*, int>> points_of_trees;

        /** 
         * The training points.
         *
         * Those points are owned by the Forest
         * @todo Store these points in a tree to implement efficient sampling
         */
        std::multiset<Point*, point_ptr_compare> points;

        /// Maximal height of the Trees
        size_t max_height;

        /// Number of points in each tree's sample
        size_t samples_size;

        /// Epsilon parameter, used for deciding when to rebuild a Vertex of a Tree
        float epsilon;
        
        /// Min number of points that a Vertex should contain to not be a leaf
        unsigned int min_split_points;
        
        /// Min value that a Vertex should match to not be a leaf
        float min_split_gini;
        
        /**
         * Epsilon value to use when figuring out which Vertex to recompute
         *
         * Epsilon is used twice in the algorithm : once to decide whether the
         * vertex should be recalculated, and a second time when it has been
         * decided that the vertex should be recalculated, which parent of the
         * vertex or the vertex itself should be recalculate (see the paper for
         * more details). This parameter allow to have a different value of
         * epsilon for the second purpose.
         *
         * @note In most of the cases, this is expected to be equal to
         *		1, although the implementation is able to handle different value
         */
        float epsilon_transmission;
        
        /**
         * Number of features to sample when computing Gini gain
         *
         * If 0, no sampling will be made and every relevent features will be considered
         * at each step.
         **/
         size_t feat_samp_size;

        /**
         * Types of the features of the points
         */
        std::vector<FeatureType> features_types;

        /// Randomly add given number of points to tree with replacement
        void complete_tree(Tree& tree_to_complete, std::map<Point*, int>& map_of_tree, size_t number_to_add);

    public:
        /**
         * Main constructor of RandomForest
         *
         * @param list_of_points A multiset of the points that should be
         *	included in the decision tree. The ownership will be taken by the
         *      RandomForest object.
         * @param nb_trees The number of decision Trees to build
         * @param samples_size The size of the samples of points to build for
                training each Tree.
         * @param max_height The maximal height of the decision Trees
         * @param epsilon The epsilon parameter of the algorithm
         * @param min_split_points The minimal number of points a Vertex should
         * 	contain to not be a leaf
         * @param min_split_gini The minimal gini value a Vertex should match to
         *	not be a leaf
         * @param epsilon_transmission Epsilon value to use when figuring out
         * 	which vertex to recompute.
         * 	Epsilon is used twice in the algorithm : once to decide whether the
         * 	vertex should be recalculated, and a second time when it has been
         * 	decided that the vertex should be recalculated, which parent of the
         * 	vertex or the vertex itself should be recalculate (see the paper for
         * 	more details). This parameter allow to have a different value of
         * 	epsilon for the second purpose.
         * 	In most of the cases, this is expected to be equal to
         *	@p epsilon, although the implementation is able to deal with a
         * 	different value.
         * @param features_types A vector of #FeatureType that indicates, for
         *	each dimension of the features space, if the feature is boolean,
         *	classified or real.
         * @param feat_samp_size The number of features to sample when computing
         *      Gini gain. If 0, no sampling will be performed and all relevent
         *      features will be considered.
         */
        RandomForest(std::multiset<Point*> list_of_points, unsigned int nb_trees, unsigned int samples_size,
             unsigned int max_height, float epsilon, unsigned int min_split_points, float min_split_gini,
             float epsilon_transmission, std::vector<FeatureType> features_types, size_t feat_samp_size=0);

        /**
         * Copy constructor
         *
         * All parameters are copied from source, and the points are copied so that
         * the new Forest has ownership of the new points.
         *
         * @param source The RandomForest to copy
         * @see RandomForest#RandomForest(std::multiset<Point*>, unsigned int, unsigned int, unsigned int, float, unsigned int, float, float, std::vector<FeatureType>, size_t)
         */
        RandomForest(const RandomForest& source);

        /**
         * Enhanced copy constructor
         *
         * When making several tests with only the epsilon value changing, this
         * is usefull to avoid going through the costly creation process several
         * times.
         *
         * All parameters except epsilon and epsilon_transmission are copied
         * from source, and the points are copied so that the new Tree has
         * ownership of the new points
         *
         * @warning When the features studied by each tree are drawn randomly,
         * this constructor does not compute them again. It should therefore be used
         * with caution when making experiments.
         * @param source The forest from which parameters will be copied
         * @param epsilon The new epsilon value
         * @param epsilon_transmission The new epsilon_transmission value
         * @see Tree#Tree(std::multiset<Point*>, size_t, unsigned int, float, unsigned int, float, float, std::vector<FeatureType>)
         */
        RandomForest(const RandomForest& source, const float& epsilon, const float& epsilon_transmission);

        /**
         * RandomForest class assignment operator.
         *
         * Copy the data from source. The points are copied so that the new RandomForest
         * have ownership of them.
         *
         * @param source The RandomForest to copy
         * @see RandomForest#RandomForest(std::multiset<Point*>, unsigned int, unsigned int, unsigned int, float, unsigned int, float, float, std::vector<FeatureType>, size_t)
         */
        RandomForest& operator=(const RandomForest& source);

        /**
         * Add a point to the RandomForest
         *
         * A new point is created using the arguments. This may trigger rebuild
         * of part or all of the Trees if conditions are matched
         *
         * @param features Features of the point.
         * 	Memory is copy, hence this does not take ownership of the parameter
         * @param value The decision value of the point
         * @see Point#Point(size_t, const float*, const bool)
         * @see Tree#add_point(const float* features, bool values)
         * @todo Add possibility to change sample size
         * @todo Centralize all add/remove points to avoid spurious rebuilding
         */
        void add_point(const float* features, bool value);

        /**
         * Add an already constructed point to the RandomForest
         *
         * This may trigger rebuild of part or all of the Trees if
         * conditions matched
         *
         * @param to_add The point to add, the RandomForest takes ownership of it
         */
        void add_point(Point* to_add);

        /**
         * Add a point to the RandomForest
         *
         * This may trigger rebuild of part or all of the Trees if
         * conditions matched
         *
         * @param to_add The point to add, it will be copied
         */
        void add_point(const Point& to_add);

        /**
         * Delete a point from the RandomForest
         *
         * Search for the first point of the forest matching the provided features
         * and value, and delete it from the forest. This may trigger rebuild
         * of part or all of the Trees if conditions are matched
         *
         * @param features Features of the point to delete. Those are only
         *	compared to the ones of the points, and hence no ownership is taken
         * @param value Decision value of the point to delete
         * @throw std::runtime_error When no point matching criteria has been
         *	found
         */
        void delete_point(const float* features, bool value);

        /**
         * Delete a point from the RandomForest
         *
         * Search for the first point of the forest matching the features and
         * value of the provided point and delete it from the forest. This may
         * trigger rebuild of part or all of the Trees if conditions are
         * matched
         *
         * @param to_delete Point with same features and value as the one to
         *	remove
         * @throw std::runtime_error When no point matching criteria has been
         *	found
         * @todo Make to_delete constant (seems to conflict with map.find)
         */
        void delete_point(Point& to_delete);

        /**
         * Destructor of RandomForest
         *
         * Free memory of Points
         * @todo Will also free Trees when they will be pointers
         */
         ~RandomForest();

        /**
         * Create string representing the Forest
         *
         * @see Tree#to_string
         * @see Vertex#to_string
         */
        std::string to_string();

        /**
         * Get the decision of the Forest for given features.
         *
         * The decision is the majority decision among the trees. In case of
         * a draw, the decision "false" will be chosen by default.
         * @param features Features for which a decision has to be made. No
         *	ownership is taken.
         */
        bool decision(const float* features);

        /**
         * Proportion of positive votes
         *
         * This is the proportion of Trees that would give the decision
         * "true" for the given features.
         * @param features Features for which a decision has to be made. No
         *	ownership is taken.
         */
        float get_positive_proportion(const float* features);

        /**
         * Get the training error for each Tree
         *
         * This is the absolute number of points in the Tree that would not be
         * associated with the right decision if evaluated.
         */
        std::vector<unsigned int> get_training_errors();
};
#endif // RANDOMFOREST_H_INCLUDED
