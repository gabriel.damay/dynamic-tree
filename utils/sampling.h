/**
 * @file sampling.h
 * Implement fundamental functions to perform sampling
 */
#ifndef UTILS_SAMPLING_H
#define UTILS_SAMPLING_H
#include <vector>
#include <random>
#include <numeric>
#include <iterator>
#include <algorithm>
#include <string>
/**
 * Draw a sampling of int values without replacement
 *
 * Draw \p sample_size \c int values between \p min and \p max (both included)
 * without replacement (i.e. each value can appear at most once and each value
 * has the same probability of being in the sample)
 * @param min The smallest \c int value that the set can contain (included)
 * @param max The biggest \c int value that the set can contain (included)
 * @param sample_size The number of values to draw
 * @return A vector of \c int containing the values drawn
 * @throw std::invalid_argument if the range is not big enough to draw the
 *  requested number of values (i.e. <tt>\p max - \p min + 1 < \p sample_size</tt>)
 */
inline std::vector<int> sample_without_replacement(int min, int max, size_t sample_size)
{
    std::random_device device;
    std::default_random_engine gen(device());
    std::vector<int> draw_among(max-min+1, 0), result_vector;
    if(draw_among.size() < sample_size)
        throw std::invalid_argument("The range [" + std::to_string(min)
                                    + ", " + std::to_string(max)
                                    + "] is too small to draw "
                                    + std::to_string(sample_size) + " points from");
    std::iota(draw_among.begin(), draw_among.end(), min);
    std::sample(draw_among.begin(), draw_among.end(), std::back_insert_iterator(result_vector), sample_size, gen);
    return result_vector;
}
#endif // UTILS_SAMPLING_H
