/**
 * @file maps.h
 * Implement usefull functions for dealing with maps
 */
#include <map>

/**
 * Merge two map by summing the value for common keys
 *
 * The keys of the resulting map will be the union of the keys of
 * both input maps, and the values will be the sum of the two values
 * for keys that are in both maps, and the single known value for keys
 * that are in only one map
 * @tparam TKey The type of the keys in the map
 * @tparam TValue The type of values in the map. Should implement operator+
 * @tparam Compare The comparison type of the keys, usually deduced from \p TKey
 * @param map1 The first map to merge
 * @param map2 The second map to merge
 * @return A map corresponding to the two input maps merged
 * @warning The value type of the maps should implement the operator+. If
 * not, it will result in a compilation error.
 * @todo Rewrite to use the inplace function
 */
template <typename TKey, typename TValue, typename Compare = std::less<TKey>>
std::map<TKey, TValue, Compare> merge_sum(
    const std::map<TKey, TValue, Compare>& map1,
    const std::map<TKey, TValue, Compare>& map2)
{
    std::map<TKey, TValue, Compare> res_map;
    auto it1 = map1.cbegin();
    auto it2 = map2.cbegin();
    for(; it1 != map1.cend() && it2 != map2.cend();)
        if(Compare{}(it1->first, it2->first))
        {
            res_map.insert(res_map.cend(), *it1);
            it1++;
        }
        else if(Compare{}(it2->first, it1->first))
        {
            res_map.insert(res_map.cend(), *it2);
            it2++;
        }
        else
        {
            res_map.insert(res_map.end(), {it1->first, it1->second + it2.second});
            it1++; it2++;
        }
    res_map.insert(it1, it1.cend());
    res_map.insert(it2, it2.cend());
    return res_map;
}

/**
 * Merge the second map into the first one by summing the values of common keys
 *
 * @tparam TKey The type of the keys in the map
 * @tparam TValue The type of values in the map. Should implement operator+
 * @tparam Compare The comparison type of the keys, usually deduced from \p TKey
 * @param inout_map The first map to merge
 * @param in_map The second map to merge
 * @warning The value type of the maps should implement the operator+. If
 * not, it will result in a compilation error.
 * @todo Improve this function to get the best possible complexity.
 * It probably requires to distinguish between sizes of in_map and/or do
 * sound use of upper_bound or lower_bound.
 * Maybe unordered map could help?
 */
template <typename TKey, typename TValue, typename Compare = std::less<TKey>>
void merge_sum_inplace(
    std::map<TKey, TValue, Compare>& inout_map,
    const std::map<TKey, TValue, Compare>& in_map)
{
    // TODO: Compute faster by using an hinted upper_bound. Unfortunately
    // there seem to be no way of doing this in C++20, let alone C++17,
    // without recreating the data structure (or dig deeper into friend
    // functions?)
    // Maybe there is a way to do so with insert ?
    auto it2 = in_map.cbegin();
    for(auto it1 = inout_map.begin(); it1 != inout_map.end() && it2 != in_map.cend();)
        if(Compare{}(it1->first, it2->first))
            it1++;
        else if(Compare{}(it2->first, it1->first))
        {
            inout_map.insert(it1, *it2);
            it2++;
        }
        else
        {
            it1->second += it2->second;
            it1++; it2++;
        }
    for(; it2 != in_map.cend(); it2++)
        inout_map.insert(inout_map.end(), *it2);
}

